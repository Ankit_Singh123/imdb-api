﻿using SessionDemoApp.Models;
using System.Collections.Generic;
using System.Linq;
using SessionDemoApp.Models.Request;
using SessionDemoApp.Models.Response;
using SessionDemoApp.Repository;

namespace SessionDemoApp.Services
{
    public class ActorService : IActorService
    {
        private readonly IActorRepository _actorRepository;

        public ActorService(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
        }

        public IEnumerable<ActorResponse> GetAll()
        {
            return _actorRepository.GetAll().Select(a => new ActorResponse
            {
                Name = a.Name,
                Bio = a.Bio,
                Dob = a.Dob,
                Id = a.Id
            });
        }

        public void Add(Actor actor)
        {
            _actorRepository.Add(new Actor
            {
                Name = actor.Name,
                Bio = actor.Bio,
                Dob = actor.Dob,
                Id = actor.Id,
                Gender=actor.Gender
            });
        }

        public ActorResponse GetById(int id)
        {
            var v = _actorRepository.GetById(id);
            var values = new ActorResponse
            {
                Name = v.Name,
                Bio = v.Bio,
                Dob = v.Dob,
                Id = v.Id
            };
            return values;
        }

        public void Delete(int id)
        {
            _actorRepository.Delete(id);
        }

        public void Update(int id,Actor actor)
        {
            _actorRepository.Update(id,actor);
        }
    }
}