﻿using SessionDemoApp.Models;
using System.Collections.Generic;
using System.Linq;
using SessionDemoApp.Models.Request;
using SessionDemoApp.Models.Response;
using SessionDemoApp.Repository;

namespace SessionDemoApp.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;

        public GenreService(IGenreRepository genreRepository)
        {
            _genreRepository = genreRepository;
        }

        public void Add(Genre genre)
        {
            _genreRepository.Add(genre);
        }

        public void Delete(int id)
        {
            _genreRepository.Delete(id);
        }

        public IEnumerable<GenreResponse> GetAll()
        {
            return _genreRepository.GetAll().Select(a => new GenreResponse
            {
                Name = a.Name,
                Id = a.Id
            });
        }

        public GenreResponse GetById(int id)
        {
            var v = _genreRepository.GetById(id);
            var values = new GenreResponse
            {
                Name = v.Name,
                Id = v.Id
            };
            return values;
        }

        public void Update(int id, Genre genre)
        {
            _genreRepository.Update(id, genre);
        }
    } 
}