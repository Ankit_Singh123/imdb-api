﻿using SessionDemoApp.Models;
using System.Collections.Generic;
using System.Linq;
using SessionDemoApp.Models.Request;
using SessionDemoApp.Models.Response;
using SessionDemoApp.Repository;

namespace SessionDemoApp.Services
{
    public class ProducerService : IProducerService
    {
        private readonly IProducerRepository _producerRepository;

        public ProducerService(IProducerRepository producerRepository)
        {
            _producerRepository = producerRepository;
        }

        public IEnumerable<ProducerResponse> GetAll()
        {
            return _producerRepository.GetAll().Select(a => new ProducerResponse
            {
                Name = a.Name,
                Bio = a.Bio,
                Dob = a.Dob,
                Id = a.Id
            });
        }

        public void Add(Producer producer)
        {
            _producerRepository.Add(new Producer
            {
                Name = producer.Name,
                Bio = producer.Bio,
                Dob = producer.Dob,
                Id = producer.Id,
                Gender= producer.Gender
            });
        }

        public ProducerResponse GetById(int id)
        {
            var v = _producerRepository.GetById(id);
            var values = new ProducerResponse
            {
                Name = v.Name,
                Bio = v.Bio,
                Dob = v.Dob,
                Id = v.Id
            };
            return values;
        }

        public void Delete(int id)
        {
            _producerRepository.Delete(id);
        }

        public void Update(int id,Producer producer)
        {
            _producerRepository.Update(id,producer);
        }
    }
}