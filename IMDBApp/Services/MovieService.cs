﻿using SessionDemoApp.Models;
using System.Collections.Generic;
using System.Linq;
using SessionDemoApp.Models.Request;
using SessionDemoApp.Models.Response;
using SessionDemoApp.Repository;

namespace SessionDemoApp.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IActorRepository _actorRepository;
        private readonly IGenreRepository _genreRepository;
        private readonly IProducerRepository _producerRepository;

        public MovieService(IMovieRepository movieRepository, IActorRepository actorRepository, IGenreRepository genreRepository, IProducerRepository producerRepository)
        {
            _movieRepository = movieRepository;
            _actorRepository = actorRepository;
            _genreRepository = genreRepository;
            _producerRepository = producerRepository;
        }

        public int Add(MovieRequest movieRequest)
        {
           return _movieRepository.Add(movieRequest);
        }

        public void Delete(int id)
        {
            _movieRepository.Delete(id);
        }

        public MovieResponse Get(int id)
        {
            var movie = _movieRepository.GetById(id);
            Producer producer = _producerRepository.GetById(movie.ProducerId);
            ProducerResponse producerResponse = new ProducerResponse()
            {
                Id = producer.Id,
                Name = producer.Name,
                Dob = producer.Dob,
                Bio = producer.Bio
            };

            var actorResponse = _actorRepository.GetByMovieId(movie.Id).Select(a => new ActorResponse
            {
                Name = a.Name,
                Bio = a.Bio,
                Dob = a.Dob,
                Id = a.Id
            });

            var genre = _genreRepository.GetByMovieId(movie.Id).Select(a => new GenreResponse
            {
                Name = a.Name,
                Id = a.Id
            });

            var movieResponse = new MovieResponse()
            {
                Id = movie.Id,
                Name = movie.Name,
                Plot = movie.Plot,
                YOF = movie.YearOfRelease,
                Poster = movie.Poster,
                Producer = producerResponse,
                Actors = actorResponse,
                Genre = genre
            };
            return movieResponse;
        }

        public IEnumerable<MovieResponse> GetAll()
        {
       
            IEnumerable<MovieResponse> movies = _movieRepository.GetAll().Select(movie =>
            {
                Producer producer = _producerRepository.GetById(movie.ProducerId);
                ProducerResponse producerResponse = new ProducerResponse()
                {
                    Id = producer.Id,
                    Name = producer.Name,
                    Dob = producer.Dob,
                    Bio = producer.Bio
                };

                var actorResponse = _actorRepository.GetByMovieId(movie.Id).Select(a => new ActorResponse
                {
                    Name = a.Name,
                    Bio = a.Bio,
                    Dob = a.Dob,
                    Id = a.Id
                });

                var genre = _genreRepository.GetByMovieId(movie.Id).Select(a => new GenreResponse
                {
                    Name = a.Name,
                    Id = a.Id
                });

                var movieResponse = new MovieResponse()
                {
                    Id = movie.Id,
                    Name = movie.Name,
                    Plot = movie.Plot,
                    YOF = movie.YearOfRelease,
                    Poster = movie.Poster,
                    Producer = producerResponse,
                    Actors = actorResponse,
                    Genre = genre
                };
                return movieResponse;
           
            });
            return movies;
        }

        public void Update(int id, MovieRequest movieRequest)
        {
            _movieRepository.Update(id,movieRequest);
        }
    }
}
       