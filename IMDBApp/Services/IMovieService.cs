﻿using System.Collections.Generic;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Request;
using SessionDemoApp.Models.Response;

namespace SessionDemoApp.Services
{
    public interface IMovieService
    {
        public IEnumerable<MovieResponse> GetAll();
        public int Add(MovieRequest movieRequest);
        public MovieResponse Get(int id);
        public void Delete(int id);
        public void Update(int id, MovieRequest movieRequest);

    }
}
