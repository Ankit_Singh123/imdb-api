﻿using System.Collections.Generic;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Request;
using SessionDemoApp.Models.Response;

namespace SessionDemoApp.Services
{
    public interface IActorService
    {
        public IEnumerable<ActorResponse> GetAll();
        public void Add(Actor actor);
        public ActorResponse GetById(int id);
        public void Delete(int id);
        public void Update(int id,Actor actor);

    }
}
