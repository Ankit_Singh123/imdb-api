﻿using System.Collections.Generic;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Request;
using SessionDemoApp.Models.Response;

namespace SessionDemoApp.Services
{
    public interface IProducerService
    {
        public IEnumerable<ProducerResponse> GetAll();
        public void Add(Producer producer);
        public ProducerResponse GetById(int id);
        public void Delete(int id);
        public void Update(int id,Producer producer);

    }
}
