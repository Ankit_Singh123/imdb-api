﻿using System.Collections.Generic;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Request;
using SessionDemoApp.Models.Response;

namespace SessionDemoApp.Services
{
    public interface IGenreService
    {
        public IEnumerable<GenreResponse> GetAll();
        public void Add(Genre genre);
        public GenreResponse GetById(int id);
        public void Delete(int id);
        public void Update(int id, Genre genre);

    }
}
