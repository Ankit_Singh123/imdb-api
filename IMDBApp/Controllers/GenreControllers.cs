﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Request;
using SessionDemoApp.Services;

namespace SessionDemoApp.Controllers
{
    [ApiController]
    [Route("genre")]
    public class GenreController : Controller
    {
        private readonly IGenreService _genreService;

        public GenreController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        [HttpGet]
        [Route("all")]
        public IActionResult GetAll()
        {
            var actors = _genreService.GetAll();
            return Ok(actors);
        }

        [HttpPost]
        [Route("add")]
        public IActionResult Add([FromBody] Genre genre)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _genreService.Add(genre);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var genre = _genreService.GetById(id);
            if (genre == null)
            {
                return NotFound("No Record Found");
            }
            return Ok(genre);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _genreService.Delete(id);
            return Ok("Genre Deleted");
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Genre genre)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _genreService.Update(id,genre);
            return Ok("Genre Updated");
        }
    }
}