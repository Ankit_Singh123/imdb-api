﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Request;
using SessionDemoApp.Services;

namespace SessionDemoApp.Controllers
{
    [ApiController]
    [Route("producer")]
    public class ProducerController : Controller
    {
        private readonly IProducerService _producerService;

        public ProducerController(IProducerService producerService)
        {
            _producerService = producerService;
        }

        [HttpGet]
        [Route("all")]
        public IActionResult GetAll()
        
        {
            var producers = _producerService.GetAll();
            return Ok(producers);
        }

        [HttpPost]
        [Route("add")]
        public IActionResult Add([FromBody] Producer producer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _producerService.Add(producer);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var producer = _producerService.GetById(id);
            if (producer == null)
            {
                return NotFound("No Record Found");
            }
            return Ok(producer);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _producerService.Delete(id);
            return Ok("Producer Deleted");
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Producer producer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _producerService.Update(id,producer);
            return Ok("Producer Updated");
        }
    }
}