﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Request;
using SessionDemoApp.Services;

namespace SessionDemoApp.Controllers
{
    [ApiController]
    [Route("movie")]
    public class MovieController : Controller
    {
        private readonly IMovieService _movieService;

        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet]
        [Route("all")]
        public IActionResult GetAll()
        {
            var movies = _movieService.GetAll();
            return Ok(movies);
        }

        [HttpPost]
        [Route("add")]
        public IActionResult Add([FromBody] MovieRequest movierequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var  id= _movieService.Add(movierequest);
            return StatusCode(StatusCodes.Status201Created, new { id = id });
            
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var movie = _movieService.Get(id);
            if (movie == null)
            {
                return NotFound("No Record Found");
            }
            return Ok(movie);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _movieService.Delete(id);
            return Ok("Movie Deleted");
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] MovieRequest movieRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _movieService.Update(id,movieRequest);
            return Ok("Movie Updated");
        }
    }
}