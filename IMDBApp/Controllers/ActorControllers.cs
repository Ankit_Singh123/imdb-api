﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Request;
using SessionDemoApp.Services;

namespace SessionDemoApp.Controllers
{
    [ApiController]
    [Route("actor")]
    public class ActorController : Controller
    {
        private readonly IActorService _actorService;

        public ActorController(IActorService actorService)
        {
            _actorService = actorService;
        }

        [HttpGet]
        [Route("all")]
        public IActionResult GetAll()
        {
            var actors = _actorService.GetAll();
            return Ok(actors);
        }

        [HttpPost]
        [Route("add")]
        public IActionResult Add([FromBody] Actor actor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _actorService.Add(actor);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var actor = _actorService.GetById(id);
            if (actor == null)
            {
                return NotFound("No Record Found");
            }
            return Ok(actor);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _actorService.Delete(id);
            return Ok("Actor Deleted");
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Actor actor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _actorService.Update(id,actor);
            return Ok("Actor Updated");
        }
    }
}