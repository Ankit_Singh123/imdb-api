﻿using System;

namespace SessionDemoApp.Models
{
    public class GenreResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
    }
}
