﻿using System;
using System.Collections.Generic;

namespace SessionDemoApp.Models.Response
{
    public class MovieResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Plot { get; set; }
        public int YOF { get; set; }
        public string Poster { get; set; }
        public ProducerResponse Producer { get; set; }
        public IEnumerable<ActorResponse> Actors { get; set; }
        public IEnumerable<GenreResponse> Genre { get; set; }


    }
}
