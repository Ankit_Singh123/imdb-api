﻿using System;

namespace SessionDemoApp.Models.Request
{
    public class ProducerRequest
    {
        public string Name { get; set; }
        public string Bio { get; set; }
        public DateTime Dob { get; set; }
    }
}
