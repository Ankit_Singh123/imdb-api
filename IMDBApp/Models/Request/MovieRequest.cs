﻿using System;
using System.Collections.Generic;

namespace SessionDemoApp.Models.Request
{
    public class MovieRequest
    {
        public string Name { get; set; }
        public string Plot { get; set; }
        public string Poster { get; set; }
        public int YOF { get; set; }
        public int producerId { get; set; }
        public List<int> ActorIds { get; set; }
        public List<int> GenreIds { get; set; }

    }
}
