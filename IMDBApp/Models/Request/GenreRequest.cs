﻿using System;

namespace SessionDemoApp.Models
{
    public class GenreRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
    }
}
