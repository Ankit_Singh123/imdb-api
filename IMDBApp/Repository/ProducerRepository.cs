﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Microsoft.Extensions.Options;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Response;

namespace SessionDemoApp.Repository
{
    public class ProducerRepository : IProducerRepository
    {

        private readonly ConnectionString _dbConnection;

        public ProducerRepository(IOptions<ConnectionString> dbConnection)
        {
            _dbConnection = dbConnection.Value;
        }
        /// <inheritdoc />
        public IEnumerable<Producer> GetAll()
        {
            string sql = @"
SELECT ID [Id]
, Name [Name]
, DOB [DOB]
, BIO [Bio]
, Gender [Gender]
FROM Producers ";
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                return connection.Query<Producer>(sql);
            }
        }

        /// <inheritdoc />
        public void Add(Producer entity)
        {

            string sql = @"
INSERT INTO Producers (
Name
, DOB
, BIO
, Gender
)
VALUES (
@Name
, @DOB
, @Bio
, @Gender
)
";
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                connection.Query<Producer>(sql, new
                {
                    entity.Name,
                    entity.Dob,
                    entity.Bio,
                    entity.Gender
                }
                );
            }
        }

        public Producer GetById(int id)
        {
            string sql = @"
SELECT ID [Id]
, Name [Name]
, DOB [DOB]
, BIO [Bio]
, Gender [Gender]
FROM Producers
WHERE Id = @Id";
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                return connection.Query<Producer>(sql, new { Id = id }).FirstOrDefault();
            }
        }

        public void Delete(int id)
        {
            string sql = @"
DELETE 
FROM Producers
WHERE Id = @Id";
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                connection.Query<Producer>(sql, new { Id = id });
            }
        }

        public void Update(int id,Producer entity)
        {
            string sql2 = @$"
UPDATE Producers 
SET
Name=@Name
, DOB= @DOB
, BIO=@BIO
, Gender= @Gender
WHERE
ID={id}
";           using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                connection.Query<Producer>(sql2, entity);
            }
           
        }
    }
}
