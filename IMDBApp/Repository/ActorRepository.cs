﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Microsoft.Extensions.Options;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Response;

namespace SessionDemoApp.Repository
{
    public class ActorRepository : IActorRepository
    {

        private readonly ConnectionString _dbConnection;

        public ActorRepository(IOptions<ConnectionString> dbConnection)
        {
            _dbConnection = dbConnection.Value;
        }
        /// <inheritdoc />
        public IEnumerable<Actor> GetAll()
        {
            string sql = @"
SELECT ID [Id]
, Name [Name]
, DOB [DOB]
, BIO [Bio]
, Gender [Gender]
FROM Actors";
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                return connection.Query<Actor>(sql);
            }
        }

        /// <inheritdoc />
        public void Add(Actor entity)
        {

            string sql = @"
INSERT INTO Actors (
Name
, DOB
, BIO
, Gender
)
VALUES (
@Name
, @DOB
, @Bio
, @Gender
)
";
            {
                using (var connection = new SqlConnection(_dbConnection.IMDBDB))
                {
                    connection.Query<Actor>(sql, new
                    {
                        entity.Name,
                        entity.Dob,
                        entity.Bio,
                        entity.Gender
                    }
                );
                }
            }

        }

        public Actor GetById(int id)
        {
            string sql = @"
SELECT ID [Id]
, Name [Name]
, DOB [DOB]
, BIO [Bio]
, Gender [Gender]
FROM Actors
WHERE Id = @Id";
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                return connection.Query<Actor>(sql, new { Id = id }).FirstOrDefault();
            }
        }

        public void Delete(int id)
        {
            string sql = @"
DELETE FROM Actor_Movie
WHERE ActorID = @Id
DELETE 
FROM Actors
WHERE Id = @Id";
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                connection.Query<Actor>(sql, new { Id = id });
            }
        }

        public void Update(int id,Actor entity)
        {
            string sql2 = @$"
UPDATE Actors 
SET
Name=@Name
, DOB= @DOB
, BIO=@BIO
, Gender= @Gender
WHERE
ID={id}
";          using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {

                connection.Query<Actor>(sql2, entity);

            }

        }

        public IEnumerable<Actor> GetByMovieId(int id)
        {
            string sql = @"
SELECT
A.ID,
A.Name,
A.BIO,
A.DOB,
A.Gender
FROM Actors A
INNER JOIN Actor_Movie AM on
AM.ActorID=A.ID
WHERE AM.MovieID=@Id;
";
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                return connection.Query<Actor>(sql, new { Id = id });
            }
        }
    }
}
