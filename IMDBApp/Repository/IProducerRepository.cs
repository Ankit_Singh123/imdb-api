﻿using System.Collections.Generic;
using SessionDemoApp.Models;

namespace SessionDemoApp.Repository
{
    public interface IProducerRepository
    {
        public IEnumerable<Producer> GetAll();
        public void Add(Producer producer);
        public Producer GetById(int id);
        public void Delete(int id);
        public void Update(int id, Producer producer);




    }
}
