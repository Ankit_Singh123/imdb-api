﻿using System.Collections.Generic;
using SessionDemoApp.Models;

namespace SessionDemoApp.Repository
{
    public interface IGenreRepository
    {
        public IEnumerable<Genre> GetAll();
        public void Add(Genre genre);
        public Genre GetById(int id);
        public void Delete(int id);
        public void Update(int id,Genre genre);
        public IEnumerable<Genre> GetByMovieId(int id);

    }
}
