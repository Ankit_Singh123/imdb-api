﻿using System.Collections.Generic;
using SessionDemoApp.Models;

namespace SessionDemoApp.Repository
{
    public interface IActorRepository
    {
        public IEnumerable<Actor> GetAll();
        public void Add(Actor actor);
        public Actor GetById(int id);
        public void Delete(int id);
        public void Update(int id,Actor actor);
        public IEnumerable<Actor> GetByMovieId(int id);




    }
}
