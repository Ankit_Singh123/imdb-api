﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Microsoft.Extensions.Options;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Response;

namespace SessionDemoApp.Repository
{
    public class GenreRepository : IGenreRepository
    {

        private readonly ConnectionString _dbConnection;

        public GenreRepository(IOptions<ConnectionString> dbConnection)
        {
            _dbConnection = dbConnection.Value;
        }

        public void Add(Genre genre)
        {
            string sql = @"
INSERT INTO Genres (
Name
)
VALUES (
@Name
)
";         {
                using (var connection = new SqlConnection(_dbConnection.IMDBDB))
                {
                    connection.Query<Genre>(sql, genre);
                }
            }
        }

        public void Delete(int id)
        {
            string sql = @"
DELETE FROM Genre_Movie
WHERE GenreID = @Id
DELETE 
FROM Genres
WHERE Id = @Id";
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                connection.Query<Genre>(sql, new { Id = id });
            }
        }

        public IEnumerable<Genre> GetAll()
        {
            string sql = @"
SELECT ID [Id]
, Name [Name]
From Genres";
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {

                return connection.Query<Genre>(sql);
            }
        }

        public Genre GetById(int id)
        {
            string sql = @"
SELECT ID [Id]
, Name [Name]
From Genres
WHERE Id = @Id";
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                return connection.Query<Genre>(sql, new { Id = id }).FirstOrDefault();
            }
        }

        public IEnumerable<Genre> GetByMovieId(int id)
        {
            string sql = @"
SELECT
G.ID,
G.Name
FROM Genres G
INNER JOIN Genre_Movie GM on 
GM.GenreID=G.ID
WHERE GM.MovieID=@Id;
";              using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                return connection.Query<Genre>(sql, new { Id = id });
            }
        }

        public void Update(int id, Genre genre)
        {
            string sql2 = @$"
UPDATE Genres 
SET
Name=@Name
WHERE
ID={id}"; using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                connection.Query<Genre>(sql2, genre);
            }
        }
    }
}
        