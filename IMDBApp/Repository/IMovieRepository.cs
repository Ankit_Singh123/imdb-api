﻿using System.Collections.Generic;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Request;

namespace SessionDemoApp.Repository
{
    public interface IMovieRepository
    {
        public IEnumerable<Movie> GetAll();
        public int Add(MovieRequest movieRequest);
        public Movie GetById(int id);
        public void Delete(int id);
        public void Update(int id, MovieRequest movieRequest);

    }
}
