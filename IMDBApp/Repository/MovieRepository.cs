﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Request;

namespace SessionDemoApp.Repository
{
    public class MovieRepository : IMovieRepository
    {
        private readonly ConnectionString _dbConnection;

        public MovieRepository(IOptions<ConnectionString> dbConnection)
        {
            _dbConnection = dbConnection.Value;
        }

        public int Add(MovieRequest movieReuest)
        {
            var values = new
            {
                Name =movieReuest.Name,
                YearOfRelease = movieReuest.YOF,
                Plot =movieReuest.Plot,
                Poster =movieReuest.Poster,
                ProducerId =movieReuest.producerId,
                ActorsId =string.Join(',',movieReuest.ActorIds),
                GenresId = string.Join(',', movieReuest.GenreIds)
            };

            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                var id = connection.QueryFirst<int>("INSERT_MOVIE", values, commandType: CommandType.StoredProcedure);
                return id;
            }
        }

        public void Delete(int MovieId)
        {
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                connection.Execute("DeleteMovie", new { MovieId = MovieId }, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Movie> GetAll()
        {
            string sql = @"
SELECT
M.ID,
M.Name,
M.Plot,
M.Poster,
M.ProducerID,
M.YearOfRelease
FROM Movies M
";
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                return connection.Query<Movie>(sql);
            }
        }

        public Movie GetById(int id)
        {
            string sql = @" SELECT
M.ID,
M.Name,
M.Plot,
M.Poster,
M.ProducerID,
M.YearOfRelease
FROM Movies M
WHERE Id = @Id";
            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                return connection.Query<Movie>(sql, new { Id = id }).FirstOrDefault();
            }
        }

        public void Update(int id, MovieRequest movieRequest)
        {
            
            var values = new
            {
                Id=id,
                Name = movieRequest.Name,
                YearOfRelease = movieRequest.YOF,
                Plot = movieRequest.Plot,
                Poster = movieRequest.Poster,
                ProducerId = movieRequest.producerId,
                ActorsId = string.Join(',', movieRequest.ActorIds),
                GenresId = string.Join(',', movieRequest.GenreIds)
            };

            using (var connection = new SqlConnection(_dbConnection.IMDBDB))
            {
                connection.Execute("MOVIE_UPDATE", values, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
