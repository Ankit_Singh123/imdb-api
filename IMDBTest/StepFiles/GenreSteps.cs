﻿using Microsoft.Extensions.DependencyInjection;
using SessionDemo.Test.MockResources;
using SessionDemoApp;
using SessionDemoApp.Services;
using TechTalk.SpecFlow;

namespace SessionDemo.Test.StepFiles

{
    [Scope(Feature = "Genre Resource")]
    [Binding]
    public class GenreSteps : BaseSteps
    {
        public GenreSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                     //Producer Repo
                     services.AddScoped(service => GenreMock.GenreRepoMock.Object);
                     services.AddScoped<IGenreService, GenreService>();
                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            GenreMock.MockGetAll();
            GenreMock.MockGetById();
            GenreMock.MockDelete();
            GenreMock.MockAdd();
            GenreMock.MockUpdate();
            GenreMock.MockGetByMovieId();
        }
    }
}