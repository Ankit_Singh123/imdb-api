﻿using Microsoft.Extensions.DependencyInjection;
using SessionDemo.Test.MockResources;
using SessionDemoApp;
using SessionDemoApp.Services;
using TechTalk.SpecFlow;

namespace SessionDemo.Test.StepFiles

{
    [Scope(Feature = "Actor Resource")]
    [Binding]
    public class ActorSteps : BaseSteps
    {
        public ActorSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                     //Producer Repo
                     services.AddScoped(service => ActorMock.ActorRepoMock.Object);
                     services.AddScoped<IActorService, ActorService>();
                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            ActorMock.MockGetAll();
            ActorMock.MockGetById();
            ActorMock.MockDelete();
            ActorMock.MockAdd();
            ActorMock.MockUpdate();
            ActorMock.MockGetByMovieId();
        }
    }
}