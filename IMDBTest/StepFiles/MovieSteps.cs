﻿using Microsoft.Extensions.DependencyInjection;
using SessionDemo.Test.MockResources;
using SessionDemoApp;
using SessionDemoApp.Services;
using TechTalk.SpecFlow;

namespace SessionDemo.Test.StepFiles

{
    [Scope(Feature = "Movie Resource")]
    [Binding]
    public class MovieSteps : BaseSteps
    {
        public MovieSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                     //Producer Repo
                     services.AddScoped(service => MovieMock.MovieRepoMock.Object);
                     services.AddScoped<IMovieService, MovieService>();

                    services.AddScoped(service => ActorMock.ActorRepoMock.Object);
                    services.AddScoped(service => ProducerMock.ProducerRepoMock.Object);
                    services.AddScoped(service => GenreMock.GenreRepoMock.Object);

                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            MovieMock.MockGetAll();
            MovieMock.MockGetById();
            MovieMock.MockDelete();
            MovieMock.MockAdd();
            MovieMock.MockUpdate();
            ProducerMock.MockGetById();
            ActorMock.MockGetByMovieId();
            GenreMock.MockGetByMovieId();
        }
    }
}