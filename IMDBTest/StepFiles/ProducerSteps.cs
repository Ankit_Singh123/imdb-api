﻿using Microsoft.Extensions.DependencyInjection;
using SessionDemo.Test.MockResources;
using SessionDemoApp;
using SessionDemoApp.Services;
using TechTalk.SpecFlow;

namespace SessionDemo.Test.StepFiles

{
    [Scope(Feature = "Producer Resource")]
    [Binding]
    public class ProducerSteps : BaseSteps
    {
        public ProducerSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                     //Producer Repo
                     services.AddScoped(service => ProducerMock.ProducerRepoMock.Object);
                     services.AddScoped<IProducerService, ProducerService>();
                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            ProducerMock.MockGetAll();
            ProducerMock.MockGetById();
            ProducerMock.MockDelete();
            ProducerMock.MockAdd();
            ProducerMock.MockUpdate();
        }
    }
}