﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using SessionDemoApp.Models;
using SessionDemoApp.Repository;

namespace SessionDemo.Test.MockResources
{
    public class GenreMock
    {
        public static readonly Mock<IGenreRepository> GenreRepoMock = new Mock<IGenreRepository>();

        public static void MockGetAll()
        {
            GenreRepoMock.Setup(repo => repo.GetAll()).Returns(ListOfGenres());
        
        }

        public static void MockGetById()
        {
            GenreRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) => ListOfGenres().Single(a => a.Id == id));
        }

        public static void MockDelete()
        {
            GenreRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }

        public static void MockAdd()
        {
            GenreRepoMock.Setup(repo => repo.Add(ListOfGenres().Single()));
        }

        public static void MockUpdate()
        {
            GenreRepoMock.Setup(repo => repo.Update(It.IsAny<int>(), ListOfGenres().Single()));
        }

        public static void MockGetByMovieId()
        {
            GenreRepoMock.Setup(repo => repo.GetByMovieId(It.IsAny<int>())).Returns((int id) => ListOfGenres());
        }

        private static IEnumerable<Genre> ListOfGenres()
        {
            return new List<Genre>
            {
                new Genre
                {
                    Id= 1,
                    Name="Action",
                }
            };
        }
    }
}