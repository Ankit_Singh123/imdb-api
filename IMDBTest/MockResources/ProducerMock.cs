﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using SessionDemoApp.Models;
using SessionDemoApp.Repository;

namespace SessionDemo.Test.MockResources
{
    public class ProducerMock
    {
        public static readonly Mock<IProducerRepository> ProducerRepoMock = new Mock<IProducerRepository>();

        public static void MockGetAll()
        {
            ProducerRepoMock.Setup(repo => repo.GetAll()).Returns(ListOfProducers());
        
        }

        public static void MockGetById()
        {
            ProducerRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) => ListOfProducers().Single(a => a.Id == id));
        }

        public static void MockDelete()
        {
            ProducerRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }

        public static void MockAdd()
        {
            ProducerRepoMock.Setup(repo => repo.Add(ListOfProducers().Single()));
        }

        public static void MockUpdate()
        {
            ProducerRepoMock.Setup(repo => repo.Update(It.IsAny<int>(), ListOfProducers().Single()));
        }
            private static IEnumerable<Producer> ListOfProducers()
        {
            return new List<Producer>
            {
                new Producer
                {
                    Id= 1,
                    Name="Ankit",
                    Bio="--",
                    Gender="Male",
                    Dob= Convert.ToDateTime("2000-10-06")
                }
            };
        }
    }
}