﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using SessionDemoApp.Models;
using SessionDemoApp.Models.Request;
using SessionDemoApp.Repository;

namespace SessionDemo.Test.MockResources
{
    public class MovieMock
    {
        public static readonly Mock<IMovieRepository> MovieRepoMock = new Mock<IMovieRepository>();
        public static readonly Mock<IActorRepository> ActorRepoMock = new Mock<IActorRepository>();
        public static readonly Mock<IProducerRepository> ProducerRepoMock = new Mock<IProducerRepository>();
        public static readonly Mock<IGenreRepository> GenreRepoMock = new Mock<IGenreRepository>();
        public static void MockGetAll()
        {
            //var producerId = ListOfMovies().Select(x=> x.ProducerId);
            //ProducerRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) => ListOfProducers().Single(a => a.Id == id));
            MovieRepoMock.Setup(repo => repo.GetAll()).Returns(ListOfMovies());
           // var producer= ProducerRepoMock.Setup(repo => repo.GetById())
        }

        public static void MockGetById()
        {
            MovieRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) => ListOfMovies().Single(a => a.Id == id));
        }

        public static void MockDelete()
        {
            MovieRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }

        public static void MockAdd()
        {
            MovieRepoMock.Setup(repo => repo.Add(MovieRequest()));
        }

        public static void MockUpdate()
        {
            MovieRepoMock.Setup(repo => repo.Update(It.IsAny<int>(), MovieRequest()));
        }
        private static IEnumerable<Movie> ListOfMovies()
        {
            return new List<Movie>
            {
                new Movie
                {
                   Id=1,
                   Name="Ankit",
                   Plot="movieReuest.Plot",
                   YearOfRelease=2017,
                   Poster="movieReuest.Poster",
                   ProducerId=1  
                }
            };
        }
        private static MovieRequest MovieRequest()
        {
            return new MovieRequest
            {
                Name = "Ankit",
                Plot = "movieReuest.Plot",
                YOF = 2017,
                Poster = "movieReuest.Poster",
                producerId = 1,
                ActorIds = new List<int>{1},
                GenreIds = new List<int> { 1 }
            };
        }
    }
}