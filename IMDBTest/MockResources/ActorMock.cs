﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using SessionDemoApp.Models;
using SessionDemoApp.Repository;

namespace SessionDemo.Test.MockResources
{
    public class ActorMock
    {
        public static readonly Mock<IActorRepository> ActorRepoMock = new Mock<IActorRepository>();

        public static void MockGetAll()
        {
            ActorRepoMock.Setup(repo => repo.GetAll()).Returns(ListOfActors());
        
        }

        public static void MockGetById()
        {
            ActorRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) => ListOfActors().Single(a => a.Id == id));
        }

        public static void MockDelete()
        {
            ActorRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }

        public static void MockAdd()
        {
            ActorRepoMock.Setup(repo => repo.Add(ListOfActors().Single()));
        }

        public static void MockUpdate()
        {
            ActorRepoMock.Setup(repo => repo.Update(It.IsAny<int>(), ListOfActors().Single()));
        }

        public static void MockGetByMovieId()
        {
            ActorRepoMock.Setup(repo => repo.GetByMovieId(It.IsAny<int>())).Returns((int id) => ListOfActors());
        }

        private static IEnumerable<Actor> ListOfActors()
        {
            return new List<Actor>
            {
                new Actor
                {
                    Id= 1,
                    Name="Ankit",
                    Bio="--",
                    Gender="Male",
                    Dob= Convert.ToDateTime("2000-10-06")
                }
            };
        }
    }
}