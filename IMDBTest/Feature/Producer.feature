﻿Feature: Producer Resource

Scenario: Get Producer All
	Given I am a client
	When I make GET Request '/producer/all'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Ankit","bio":"--","dob":"2000-10-06T00:00:00"}]'

	
Scenario: Get Producer By Id
	Given I am a client
	When I make GET Request '/producer/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Ankit","bio":"--","dob":"2000-10-06T00:00:00"}'

Scenario: Delete Producer
	Given I am a client
	When I make Delete Request '/producer/1'
	Then response code must be '200'

Scenario: Add Producer
	Given I am a client
	When I am making a post request to '/producer/add' with the following Data '{"name":"Ankit","bio":"--","dob":"2000-10-06","gender":"Male"}'
	Then response code must be '201'

Scenario: Update Producer
	Given I am a client
	When I make PUT Request '/producer/1' with the following Data with the following Data '{"name":"Ankit","bio":"--","dob":"2000-10-06","gender":"Male"}'
	Then response code must be '200'


