﻿Feature: Genre Resource

Scenario: Get Actor All
	Given I am a client
	When I make GET Request '/genre/all'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Action"}]'

	
Scenario: Get Actor By Id
	Given I am a client
	When I make GET Request '/genre/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Action"}'

Scenario: Delete Actor
	Given I am a client
	When I make Delete Request '/genre/1'
	Then response code must be '200'

Scenario: Add Actor
	Given I am a client
	When I am making a post request to '/genre/add' with the following Data '{"name":"Action"}'
	Then response code must be '201'

Scenario: Update Actor
	Given I am a client
	When I make PUT Request '/genre/1' with the following Data with the following Data '{"name":"Action"}'
	Then response code must be '200'


