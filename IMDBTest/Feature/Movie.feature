﻿Feature: Movie Resource

Scenario: Get Movie All
	Given I am a client
	When I make GET Request '/movie/all'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Ankit","plot":"movieReuest.Plot","yof":2017,"poster":"movieReuest.Poster","producer":{"id":1,"name":"Ankit","bio":"--","dob":"2000-10-06T00:00:00"},"actors":[{"id":1,"name":"Ankit","bio":"--","dob":"2000-10-06T00:00:00"}],"genre":[{"id":1,"name":"Action"}]}]'	
Scenario: Get Movie By Id
	Given I am a client
	When I make GET Request '/movie/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Ankit","plot":"movieReuest.Plot","yof":2017,"poster":"movieReuest.Poster","producer":{"id":1,"name":"Arjun","bio":"fvhgvh","dob":"1998-05-14T00:00:00"},"actors":[{"id":2,"name":"Robert DeNiro","bio":"wdghvag","dob":"1957-07-10T00:00:00"}],"genre":[{"id":2,"name":"Comedy"}]}'

Scenario: Delete Movie
	Given I am a client
	When I make Delete Request '/movie/1'
	Then response code must be '200'

Scenario: Add Movie
	Given I am a client
	When I am making a post request to '/movie/add' with the following Data '{"Name": "Ankit","Plot": "movieReuest.Plot","YOF": 2017,"Poster": "movieReuest.Poster","producerId":1,"ActorIds":[2],"GenreIds":[2]}'
	Then response code must be '201'

Scenario: Update Movie
	Given I am a client
	When I make PUT Request '/movie/1' with the following Data with the following Data '{"Name": "Ankit","Plot": "movieReuest.Plot","YOF": 2017,"Poster": "movieReuest.Poster","producerId":1,"ActorIds":[2],"GenreIds":[2]}'
	Then response code must be '200'


