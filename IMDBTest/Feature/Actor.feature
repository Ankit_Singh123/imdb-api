﻿Feature: Actor Resource

Scenario: Get Actor All
	Given I am a client
	When I make GET Request '/actor/all'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Ankit","bio":"--","dob":"2000-10-06T00:00:00"}]'

	
Scenario: Get Actor By Id
	Given I am a client
	When I make GET Request '/actor/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Ankit","bio":"--","dob":"2000-10-06T00:00:00"}'

Scenario: Delete Actor
	Given I am a client
	When I make Delete Request '/actor/1'
	Then response code must be '200'

Scenario: Add Actor
	Given I am a client
	When I am making a post request to '/actor/add' with the following Data '{"name":"Ankit","bio":"--","dob":"2000-10-06","gender":"Male"}'
	Then response code must be '201'

Scenario: Update Actor
	Given I am a client
	When I make PUT Request '/actor/1' with the following Data with the following Data '{"name":"Ankit","bio":"--","dob":"2000-10-06","gender":"Male"}'
	Then response code must be '200'


